import 'package:shelf_router/shelf_router.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_static/shelf_static.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

void main() {
  final configFile = File('config.json');
  final jsonString = configFile.readAsString();
  jsonString.then((value) {
    final dynamic jsonConfig = jsonDecode(value);
    getChannelInfo(jsonConfig['channel'], jsonConfig['api-token'], jsonConfig['port']);
    
    var pipeline = const shelf.Pipeline();

    var app = Router();
    app.get('/commands', (shelf.Request request) {

      return shelf.Response.ok(
          jsonEncode({
            'success':true,
            'data':jsonConfig['commands']
          }),
          headers: {
            'Content-type':'application/json'
          },
      );
    });
    app.all('/<ignored|.*>', createStaticHandler('server', defaultDocument: 'index.html'));


    io.serve(app, 'localhost', jsonConfig['port']);
    print('... VLT 2022');
    print('');
    print('Server started - copy url to OBS');
    print('');
  });
}

Future<void> getChannelInfo(channel, token, port) async {
  
  var queryParameters = {
    'channel_name': channel
  };
  var url = Uri.https('wasd.tv', 'api/v2/broadcasts/public', queryParameters);
  var response = await http.get(url);
  if (response.statusCode == 200) {
    final dynamic jsonResp = jsonDecode(response.body);
    final channel_id = jsonResp['result']['channel']['channel_id'];
    final stream_id = jsonResp['result']['media_container']['media_container_streams'][0]['stream_id'];
    
    var url2 = Uri.https('wasd.tv', 'api/auth/chat-token');
    var response2 = await http.post(url2, headers: {"Authorization": "Token $token"});
    if (response2.statusCode == 201) {
      final dynamic jsonResp2 = jsonDecode(response2.body);
      print('http://localhost:$port/?stream=$stream_id&channel=$channel_id&token=${jsonResp2['result']}');
    } else {
      print('api/auth/chat-token: ${response2.statusCode}.');
      return;
    }
      
      //http://localhost:3000/?stream=1106676&channel=1339436&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMzAyMDIzLCJ1c2VyX3JvbGUiOiJVU0VSIiwiaWF0IjoxNjYwNjQ0ODI5LCJleHAiOjE2NjA2NzM2Mjl9.eoVPbCSVpgJ9422g97oqOTj3MmRXsg7x6EgXBF2f77o
  } else {
      print('api/v2/broadcasts/public: ${response.statusCode}.');
      return;
    }
}
